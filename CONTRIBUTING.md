# Overview

To account for some of the typical problems related to scientific software (Romano and Moore, 2020) we briefly outline our software managment plan here.

- The [Neural Data Science and Statistical Computing Lab](https://www.cbs.mpg.de/methods-and-development-groups/neural-data-science-and-statistical-computing) of the principal investigator @nscherf will be maintaining the software in the future. 

- This software uses permissive licensing (MIT License).

- @jojoww , @nscherf and the [Neural Data Science and Statistical Computing Lab](https://www.cbs.mpg.de/methods-and-development-groups/neural-data-science-and-statistical-computing) will provide updates and bug fixes. Contributions from the community are very welcome of course. If there will be major updates, we plan to publish follow-up papers accordingly.

- We will add a DOI to the code version at time of publication of the manuscript. 

# Contributing
You are welcome to contribute to this open-source project. We took the Contributing guide https://gist.github.com/PurpleBooth/b24679402957c63ec426 by @PurpleBooth as a reference here.

## Issues

Issues are very valuable to this project.

* Ideas are a valuable source of contributions others can make
* Problems show where this project is lacking
* With a question you show where contributors can improve the user experience

Thank you for creating them.

## Pull Requests

Pull requests are a great way to get your ideas into this repository.

When deciding if we merge in a pull request I look at the following things:

### Does it state intent

You should be clear which problem you're trying to solve with your contribution.

For example:

> Add link to code of conduct in README.md

Doesn't tell me anything about why you're doing that

> Add link to code of conduct in README.md because users don't always look in the CONTRIBUTING.md

Tells me the problem that you have found, and the pull request shows me the action you have taken to solve it.

### Does it follow the contributor covenant

This repository has a [code of conduct](CODE_OF_CONDUCT.md), we will remove things that do not respect it.

## References

Romano, Joseph D., and Jason H. Moore. “Ten Simple Rules for Writing a Paper about Scientific Software.” PLOS Computational Biology 16, no. 11 (November 12, 2020): e1008390. https://doi.org/10.1371/journal.pcbi.1008390.

https://gist.github.com/PurpleBooth/b24679402957c63ec426

