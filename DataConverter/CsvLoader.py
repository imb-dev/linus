import csv
import os

from .AbstractLoader import AbstractLoader


class CsvLoader(AbstractLoader):
    """ A helper to load a folder of CSV files and to represent it by:
        - a numpy array for the positions, [n_tracks, n_pos_per_track, 3 (x/y/z)]
        - a numpy array for attributes, [n_tracks, n_pos_per_track, n_attributes]
        - a list of attribute names, derived from the header or automatically [att0, att1,...] 
    """

    def __init__(self, 
                 folderWithCSVs, 
                 resampleTo=50, 
                 minTrackLength=2, 
                 firstLineIsHeader=True, 
                 csvSeparator=",", 
                 csvIdColumn=None, 
                 csvXColumn=None, 
                 csvYColumn=None, 
                 csvZColumn=None, 
                 ignoreColumns=[],
                 dim=3):
        super(CsvLoader, self).__init__(resampleTo, minTrackLength)
        self.csvSeparator = csvSeparator
        self.csvIdColumn = csvIdColumn
        self.csvXColumn = csvXColumn
        self.csvYColumn = csvYColumn
        self.csvZColumn = csvZColumn
        self.ignoreColumns = ignoreColumns
        self.firstLineIsHeader = firstLineIsHeader
        self.loadCsvs(folderWithCSVs)
        self.convertTrackListToMatrix()

    def loadCsvs(self, folder):
        """ Calls the loading-function for each csv file """
        counter = 0
        for filename in sorted(os.listdir(folder)):
            if filename.endswith(".csv"):
                if counter == 0:
                    self.analyzeHeader(folder + "/" + filename)
                self.handleCsv(folder + "/" + filename, counter)
                counter += 1
        print()

    def analyzeHeader(self, filename):
        try:
            with open(filename) as f:
                objectReader = csv.reader(
                    f, delimiter=self.csvSeparator, skipinitialspace=True)
                row = next(objectReader)
                attributeCounter = 0
                self.csvIdColumnIndex = -1
                self.csvXColumnIndex = 0
                self.csvYColumnIndex = 1
                self.csvZColumnIndex = 2
                self.ignoreColumnIndices = []
                for i, value in enumerate(row):
                    if self.firstLineIsHeader:
                        if self.csvIdColumn is not None:
                            if value == self.csvIdColumn:
                                self.csvIdColumnIndex = i
                                continue
                        if self.csvXColumn is not None and self.csvYColumn is not None and self.csvZColumn is not None:
                            if value == self.csvXColumn:
                                self.csvXColumnIndex = i
                                continue
                            if value == self.csvYColumn:
                                self.csvYColumnIndex = i
                                continue
                            if value == self.csvZColumn:
                                self.csvZColumnIndex = i
                                continue
                        if value in self.ignoreColumns:
                            self.ignoreColumnIndices.append(i)
                            continue
                        if i >= self.dim:
                            self.attributeNames.append(value)
                    elif i >= self.dim:
                        attributeCounter += 1
                        self.attributeNames.append(
                            "Attrib" + str(attributeCounter))
        except IOError:
            self.stopBecauseMissingFile(filename, "csv file")

    def handleCsv(self, filename, counter):
        try:
            with open(filename) as f:
                self.lastTrackId = None
                counterWithinFile = 0
                self.trackList[counter] = []
                self.simpleStatusPrint(counter, 50)
                objectReader = csv.reader(
                    f, delimiter=self.csvSeparator, skipinitialspace=True)
                # skip first line if it's a header
                if self.firstLineIsHeader:
                    row = next(objectReader)
                for row in objectReader:
                    # In case a track id column was defined, make a new track whenever track id changes
                    if self.csvIdColumnIndex >= 0 and self.lastTrackId != row[self.csvIdColumnIndex]:
                        if self.lastTrackId is not None:
                            counterWithinFile += 1
                        self.lastTrackId = row[self.csvIdColumnIndex]
                        self.trackList[counter + counterWithinFile] = []
                    newRow = [row[self.csvXColumnIndex],
                              row[self.csvYColumnIndex], 
                              row[self.csvZColumnIndex]]
                    self.trackList[counter + counterWithinFile].append(newRow + 
                        [float(v) for i, v in enumerate(row) if 
                        self.csvIdColumnIndex != i and 
                        self.csvXColumnIndex != i and 
                        self.csvYColumnIndex != i and 
                        self.csvZColumnIndex != i and 
                        i not in self.ignoreColumnIndices])
        except IOError:
            self.stopBecauseMissingFile(filename, "csv file")
